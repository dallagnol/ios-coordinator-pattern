//
//  OnBoardingCoordinator.swift
//  
//
//  Created by Lorenzo DallAgnol on 17/11/22.
//

import Foundation
import UIKit
import SwiftUI

public final class OnBoardingCoordinator {
    public init(delegate: OnBoardingDelegate?) {
        self.navigationController = .init(rootViewController: .init())
        self.delegate = delegate
    }
    
    private let navigationController: UINavigationController?
    private weak var delegate: OnBoardingDelegate?
}


public extension OnBoardingCoordinator {
    func showInitial(in viewController: UIViewController) {
        guard let navigationController = self.navigationController else { return }
        
        let view = buildInitial()
        
        navigationController.setViewControllers([view], animated: true)
        navigationController.isModalInPresentation = true
        
        viewController.present(navigationController, animated: true)
    }
}

private extension OnBoardingCoordinator {
    func buildInitial() -> UIViewController {
        let outputClosure: (HelloUserOutput) -> Void = { output in
            switch output {
            case .selectedContinue:
                self.delegate?.askForGoodbye(self)
            }
        }
        let viewModel: HelloUserViewModel = .init(outputClosure: outputClosure)
        let view: HelloUserView = .init(viewModel: viewModel)
        let hosting: UIHostingController = .init(rootView: view)
        
        return hosting
    }
}

