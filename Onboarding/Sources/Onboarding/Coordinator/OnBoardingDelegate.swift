//
//  OnBoardingDelegate.swift
//  
//
//  Created by Lorenzo DallAgnol on 17/11/22.
//

import Foundation

public protocol OnBoardingDelegate: AnyObject {
    func askForGoodbye(_ coordinator: OnBoardingCoordinator)
}


