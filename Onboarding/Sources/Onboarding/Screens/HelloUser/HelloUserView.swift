//
//  OnBoardingView.swift
//  
//
//  Created by Lorenzo DallAgnol on 17/11/22.
//

import SwiftUI

struct HelloUserView: View {
    
    @ObservedObject var viewModel: HelloUserViewModel
    
    var body: some View {
        Text("hello world")
            .onTapGesture {
                viewModel.tapText()
            }
    }
}

