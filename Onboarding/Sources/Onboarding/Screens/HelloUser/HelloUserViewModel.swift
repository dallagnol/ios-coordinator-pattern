//
//  HelloUserViewModel.swift
//  
//
//  Created by Lorenzo DallAgnol on 17/11/22.
//

import Foundation

final class HelloUserViewModel: ObservableObject {
    
    public init(outputClosure: ((HelloUserOutput) -> Void)?) {
        self.outputClosure = outputClosure
    }
    
    private var outputClosure: ((HelloUserOutput) -> Void)?
}

extension HelloUserViewModel {
    func tapText() {
        outputClosure?(.selectedContinue)
    }
}
