//
//  SceneHandler.swift
//  
//
//  Created by Lorenzo DallAgnol on 10/11/22.
//

import Foundation
import UIKit

public final class SceneHandler: NSObject {
    
    // MARK: - Singleton
    
    private override init() {
        super.init()
    }
    
    public static var shared: SceneHandler = .init()
    
    // MARK: - Property
    
    private(set) var appCoordinator: AppCoordinator!
    
    // MARK: - Variables
    
    var topViewController: UIViewController {
        return UIApplication.topViewController() ?? .init()
    }
}


// MARK: - Setup

public extension SceneHandler {
    func setup(window: UIWindow) {
        setupRootViewController(window: window)
    }
}

// MARK: - Private Setup

private extension SceneHandler {
    func setupRootViewController(window: UIWindow) {
        let appCoordinator: AppCoordinator = .init(parentCoordinator: nil)
        appCoordinator.startCoordinator(window: window)
        self.appCoordinator = appCoordinator
    }
}
