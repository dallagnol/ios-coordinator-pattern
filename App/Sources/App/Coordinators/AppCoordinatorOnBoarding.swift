//
//  AppCoordinatorOnBoarding.swift
//  
//
//  Created by Lorenzo DallAgnol on 17/11/22.
//

import Foundation
import Onboarding

extension AppCoordinator {
    func showOnBoarding() {
        let coordinator: OnBoardingCoordinator = .init(delegate: self)
        coordinator.showInitial(in: topViewController)
        
        self.onBoardingCoordinator = coordinator
    }
}

extension AppCoordinator: OnBoardingDelegate {
    public func askForGoodbye(_ coordinator: OnBoardingCoordinator) {
        routeFeatures(.onExit)
    }
}
