//
//  AppCoordinator.swift
//  
//
//  Created by Lorenzo DallAgnol on 10/11/22.
//

import Foundation
import UIKit
import Onboarding
import OnExit

public protocol Coordinator {
    init(parentCoordinator: Coordinator?)
}

public enum Features {
    case onBoarding
    case onExit
}

public final class AppCoordinator: Coordinator {
    
    public init(parentCoordinator: Coordinator?) {
        self.parentCoordinator = parentCoordinator
    }
    
    private var window: UIWindow?
    private(set) var parentCoordinator: Coordinator?
    
    var onBoardingCoordinator: OnBoardingCoordinator?
    var onExitCoordinator: OnExitCoordinator?
    
    var topViewController: UIViewController {
        return UIApplication.topViewController() ?? .init()
    }
    
    func startCoordinator(window: UIWindow?) {
        self.window = window
        window?.rootViewController = .init()
        window?.makeKeyAndVisible()
        
        routeFeatures(.onBoarding)
    }
}

extension AppCoordinator {
    func routeFeatures(_ feature: Features) {
        switch feature {
        case .onBoarding:
            showOnBoarding()
            
        case .onExit:
            showOnExit()
        }
    }
}
