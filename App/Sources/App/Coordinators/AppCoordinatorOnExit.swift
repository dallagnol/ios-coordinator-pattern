//
//  AppCoordinatorOnExit.swift
//  
//
//  Created by Lorenzo DallAgnol on 17/11/22.
//

import Foundation
import OnExit

extension AppCoordinator {
    func showOnExit() {
        let coordinator: OnExitCoordinator = .init()
        coordinator.showInitial(in: topViewController)
        
        self.onExitCoordinator = coordinator
    }
}
