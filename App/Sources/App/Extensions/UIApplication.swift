//
//  UIApplicationExtensions.swift
//  
//
//  Created by Lorenzo DallAgnol on 16/11/22.
//

import Foundation
import UIKit

public extension UIApplication {
    class func topViewController(
        _ viewController: UIViewController? = UIApplication.shared.windows.first?.rootViewController
    ) -> UIViewController? {
        var topController: UIViewController? = viewController
        
        while let presentedViewController = topController?.presentedViewController {
            topController = presentedViewController
        }
        
        return topController
    }
}
