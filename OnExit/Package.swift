// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "OnExit",
    platforms: [.iOS(.v13)],
    products: [
        .library(
            name: "OnExit",
            targets: ["OnExit"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "OnExit",
            dependencies: []),
        .testTarget(
            name: "OnExitTests",
            dependencies: ["OnExit"]),
    ]
)
