//
//  OnExitCoordinator.swift
//  
//
//  Created by Lorenzo DallAgnol on 17/11/22.
//

import Foundation
import UIKit
import SwiftUI

public final class OnExitCoordinator {
    public init() {
        self.navigationController = .init(rootViewController: .init())
    }
    
    private let navigationController: UINavigationController?
}


public extension OnExitCoordinator {
    func showInitial(in viewController: UIViewController) {
        guard let navigationController = self.navigationController else { return }
        
        let view = buildGoodbyeWorld()
        
        navigationController.setViewControllers([view], animated: true)
        navigationController.isModalInPresentation = true
        
        viewController.present(navigationController, animated: true)
    }
}

private extension OnExitCoordinator {
    func buildGoodbyeWorld() -> UIViewController {
        let viewModel: GoodbyeWorldViewModel = .init()
        let view: GoodbyeWorldView = .init(viewModel: viewModel)
        let hosting: UIHostingController = .init(rootView: view)
        
        return hosting
    }
}

