import XCTest
@testable import OnExit

final class OnExitTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(OnExit().text, "Hello, World!")
    }
}
