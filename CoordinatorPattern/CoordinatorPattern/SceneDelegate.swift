//
//  SceneDelegate.swift
//  CoordinatorPattern
//
//  Created by Lorenzo DallAgnol on 17/11/22.
//

import UIKit
import App

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = scene as? UIWindowScene else { return }
        
        let window: UIWindow = .init(windowScene: windowScene)
        SceneHandler.shared.setup(window: window)
    }
}

